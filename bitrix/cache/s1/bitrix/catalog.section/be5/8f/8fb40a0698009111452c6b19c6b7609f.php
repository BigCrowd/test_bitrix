<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001449902972';
$dateexpire = '001485902972';
$ser_content = 'a:2:{s:7:"CONTENT";s:10423:"<div class="catalog-list">


<div class="catalog-item" id="bx_3966226736_27">
	<div class="catalog-item-image">
		<a href="/products/4/27/"><img border="0" src="/upload/iblock/73b/73b76be6004b3328b75e0855bea9a394.jpg" width="52" height="75" alt="Изопласт Хром" title="Изопласт Хром" /></a>
	</div>
	<div class="catalog-item-title"><a href="/products/4/27/">Изопласт Хром</a></div>
	<div class="catalog-item-desc-float">
		        <p>Популярная модель офисного стула на хромированном металлическом каркасе с пластиковыми спинкой и 
        сиденьем хорошо вписывается в любой интерьер. Прочные и долговечные, эти стулья удобны для использования 
        в качестве аудиторных конференц-кресел, кроме того, их легко хранить.</p>
      	</div>

</div>
<div class="catalog-item" id="bx_3966226736_26">
	<div class="catalog-item-image">
		<a href="/products/4/26/"><img border="0" src="/upload/iblock/543/5439232ef6153325e54ed99b4571502d.jpg" width="48" height="75" alt="Сити" title="Сити" /></a>
	</div>
	<div class="catalog-item-title"><a href="/products/4/26/">Сити</a></div>
	<div class="catalog-item-desc-float">
		        <p>Стильный, легкий и удобный стул на хромированном основании сразу привлекает внимание 
        своей необычной формой. А разнообразные варианты обивки позволяют подобрать наилучшее сочетание с 
        любой компьютерной мебелью.</p>
      	</div>

</div>
<div class="catalog-item" id="bx_3966226736_25">
	<div class="catalog-item-image">
		<a href="/products/4/25/"><img border="0" src="/upload/iblock/039/0397f4257c0a378bb01cea3323b72868.jpg" width="59" height="75" alt="Эра" title="Эра" /></a>
	</div>
	<div class="catalog-item-title"><a href="/products/4/25/">Эра</a></div>
	<div class="catalog-item-desc-float">
		        <p>Стильный стул необычный формы с жесткими сиденьями на металлическом каркасе удобен тем, 
        что можно компактно сложить для хранения до 45 таких стульев. Легкая и прочная модель отлично подойдет 
        к компьютерной стойке, также ее можно использовать как мебель для кафе и баров.</p>
      	</div>

</div>
<div class="catalog-item" id="bx_3966226736_24">
	<div class="catalog-item-image">
		<a href="/products/4/24/"><img border="0" src="/upload/iblock/3d8/3d848c989026dafd75824a1b1a7bdc06.jpg" width="49" height="75" alt="Фазенда" title="Фазенда" /></a>
	</div>
	<div class="catalog-item-title"><a href="/products/4/24/">Фазенда</a></div>
	<div class="catalog-item-desc-float">
		        <p>Складной деревянный стул с сиденьем и спинкой из плотного хлопка, изготовленный из натуральных природных материалов. 
        Ткань и дерево считаются теплыми материалами - даже зимой сидеть на них комфортнее, чем на железе или пластике. Это прекрасное решение для дачи.</p>
      	</div>

</div>
<div class="catalog-item" id="bx_3966226736_23">
	<div class="catalog-item-image">
		<a href="/products/3/23/"><img border="0" src="/upload/iblock/3db/3db99c56b06404302088719302ba826f.jpg" width="47" height="75" alt="Валенсия" title="Валенсия" /></a>
	</div>
	<div class="catalog-item-title"><a href="/products/3/23/">Валенсия</a></div>
	<div class="catalog-item-desc-float">
		        <p>Стильный дизайн стульев Валенсия сразу привлекает внимание, а эргономичная форма спинки и 
        мягкое сиденье делают их необычайно удобными. Эти стулья хорошо подойдут к любой офисной мебели для 
        персонала.</p>
      	</div>

</div>
<div class="catalog-item" id="bx_3966226736_22">
	<div class="catalog-item-image">
		<a href="/products/3/22/"><img border="0" src="/upload/iblock/0bf/0bf7b8b916af7e1918c987396c34f39f.jpg" width="45" height="75" alt="Палермо" title="Палермо" /></a>
	</div>
	<div class="catalog-item-title"><a href="/products/3/22/">Палермо</a></div>
	<div class="catalog-item-desc-float">
		        <p>Универсальный дизайн стульев Палермо позволит с успехом использовать их и в офисных помещениях, 
        и в интерьере кафе, и в домашней обстановке. Стулья Палермо добавят уюта в каждое помещение и 
        органично сольются с его стилем.</p>
      	</div>

</div>
<div class="catalog-item" id="bx_3966226736_21">
	<div class="catalog-item-image">
		<a href="/products/3/21/"><img border="0" src="/upload/iblock/a23/a230977a12e0b74364656584614ccaae.jpg" width="47" height="75" alt="Парма" title="Парма" /></a>
	</div>
	<div class="catalog-item-title"><a href="/products/3/21/">Парма</a></div>
	<div class="catalog-item-desc-float">
		        <p>Стулья Парма внесут в кухню уютное обаяние экологически чистого древесного материала. 
        Стул практически целиком сделан из массива бука, исключением стало лишь мягкое сиденье. 
        Тонировка деревянных деталей может быть выполнена в любой цветовой палитре.</p>
      	</div>

</div>
<div class="catalog-item" id="bx_3966226736_20">
	<div class="catalog-item-image">
		<a href="/products/2/20/"><img border="0" src="/upload/iblock/b42/b42af68093519685a388a9d24db74c7b.jpg" width="45" height="75" alt="Плутон" title="Плутон" /></a>
	</div>
	<div class="catalog-item-title"><a href="/products/2/20/">Плутон</a></div>
	<div class="catalog-item-desc-float">
		        <p>Офисное кресло, подобранное с учетом ваших физиологических особенностей - важная составляющая работоспособности и здоровья. 
        Данная модель будет незаменима для тех, кто много работает за компьютером.</p>
      	</div>

</div>
<div class="catalog-item" id="bx_3966226736_19">
	<div class="catalog-item-image">
		<a href="/products/2/19/"><img border="0" src="/upload/iblock/b75/b75bc0f5ff66b42eb9dff3ec80239e1c.jpg" width="54" height="75" alt="Президент" title="Президент" /></a>
	</div>
	<div class="catalog-item-title"><a href="/products/2/19/">Президент</a></div>
	<div class="catalog-item-desc-float">
		        <p><b>Кресло руководителя Президент</b> во главе большого круглого стола поможет создать в вашем кабинете
        обстановку легендарной совещательной комнаты. Ведь это не просто мебель офисная – это настоящий трон, который 
        поможет управленцу решать любые вопросы, согласуясь с понятиями чести и совести.</p> 
      	</div>

</div>
<div class="catalog-item" id="bx_3966226736_18">
	<div class="catalog-item-image">
		<a href="/products/2/18/"><img border="0" src="/upload/iblock/2f0/2f064c6f6d707bc1e9195d136ee163ff.jpg" width="74" height="75" alt="Лидер" title="Лидер" /></a>
	</div>
	<div class="catalog-item-title"><a href="/products/2/18/">Лидер</a></div>
	<div class="catalog-item-desc-float">
		        <p>Этот стильный стол абсолютно не симметричен, и это придает ему изящность и оригинальность. 
        Слева он опирается на ножку, освобождая полезное пространство для системного блока и ног. 
        Справа основанием служит удобная и вместительная тумба. Плавные линии, стильный дизайн и высокая эргономичность являются 
        основными достоинствами данной серии.</p>
      	</div>

</div>

	<br /><div class="navigation">
		<div class="navigation-arrows">
			<span class="arrow">&larr;</span><span class="ctrl"> ctrl</span>&nbsp;<span class="disabled">Пред.</span>&nbsp;<a href="/products/?PAGEN_1=2" id="navigation_1_next_page">След.</a>&nbsp;<span class="ctrl">ctrl </span><span class="arrow">&rarr;</span>
		</div>

		<div class="navigation-pages">
			<span class="navigation-title">Страницы:</span>
				<span class="nav-current-page">1</span>
				<a href="/products/?PAGEN_1=2">2</a>
				<a href="/products/?PAGEN_1=3">3</a>
		</div>
</div>
<script type="text/javascript">
	BX.bind(document, "keydown", function (event) {

		event = event || window.event;
		if (!event.ctrlKey)
			return;

		var target = event.target || event.srcElement;
		if (target && target.nodeName && (target.nodeName.toUpperCase() == "INPUT" || target.nodeName.toUpperCase() == "TEXTAREA"))
			return;

		var key = (event.keyCode ? event.keyCode : (event.which ? event.which : null));
		if (!key)
			return;

		var link = null;
		if (key == 39)
			link = BX(\'navigation_1_next_page\');
		else if (key == 37)
			link = BX(\'navigation_1_previous_page\');

		if (link && link.href)
			document.location = link.href;
	});
</script></div>
";s:4:"VARS";a:2:{s:8:"arResult";a:5:{s:2:"ID";i:0;s:15:"NAV_CACHED_DATA";N;s:16:"IPROPERTY_VALUES";a:0:{}s:16:"BACKGROUND_IMAGE";b:0;s:19:"USE_CATALOG_BUTTONS";a:0:{}}s:18:"templateCachedData";a:4:{s:9:"frameMode";b:0;s:12:"frameModeCtx";s:94:"/bitrix/templates/furniture_pale-blue/components/bitrix/catalog.section/furniture/template.php";s:8:"__NavNum";i:1;s:13:"__editButtons";a:20:{i:0;a:5:{i:0;s:13:"AddEditAction";i:1;i:27;i:2;s:163:"/bitrix/admin/cat_product_edit.php?IBLOCK_ID=2&type=products&ID=27&lang=ru&force_catalog=1&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Fproducts%2F";i:3;s:27:"Изменить товар";i:4;a:0:{}}i:1;a:5:{i:0;s:15:"AddDeleteAction";i:1;i:27;i:2;s:118:"/bitrix/admin/iblock_element_admin.php?IBLOCK_ID=2&type=products&lang=ru&action=delete&ID=27&return_url=%2Fproducts%2F";i:3;s:25:"Удалить товар";i:4;a:1:{s:7:"CONFIRM";s:76:"Вы уверены, что хотите удалить этот товар?";}}i:2;a:5:{i:0;s:13:"AddEditAction";i:1;i:26;i:2;s:163:"/bitrix/admin/cat_product_edit.php?IBLOCK_ID=2&type=products&ID=26&lang=ru&force_catalog=1&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Fproducts%2F";i:3;s:27:"Изменить товар";i:4;a:0:{}}i:3;a:5:{i:0;s:15:"AddDeleteAction";i:1;i:26;i:2;s:118:"/bitrix/admin/iblock_element_admin.php?IBLOCK_ID=2&type=products&lang=ru&action=delete&ID=26&return_url=%2Fproducts%2F";i:3;s:25:"Удалить товар";i:4;a:1:{s:7:"CONFIRM";s:76:"Вы уверены, что хотите удалить этот товар?";}}i:4;a:5:{i:0;s:13:"AddEditAction";i:1;i:25;i:2;s:163:"/bitrix/admin/cat_product_edit.php?IBLOCK_ID=2&type=products&ID=25&lang=ru&force_catalog=1&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Fproducts%2F";i:3;s:27:"Изменить товар";i:4;a:0:{}}i:5;a:5:{i:0;s:15:"AddDeleteAction";i:1;i:25;i:2;s:118:"/bitrix/admin/iblock_element_admin.php?IBLOCK_ID=2&type=products&lang=ru&action=delete&ID=25&return_url=%2Fproducts%2F";i:3;s:25:"Удалить товар";i:4;a:1:{s:7:"CONFIRM";s:76:"Вы уверены, что хотите удалить этот товар?";}}i:6;a:5:{i:0;s:13:"AddEditAction";i:1;i:24;i:2;s:163:"/bitrix/admin/cat_product_edit.php?IBLOCK_ID=2&type=products&ID=24&lang=ru&force_catalog=1&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Fproducts%2F";i:3;s:27:"Изменить товар";i:4;a:0:{}}i:7;a:5:{i:0;s:15:"AddDeleteAction";i:1;i:24;i:2;s:118:"/bitrix/admin/iblock_element_admin.php?IBLOCK_ID=2&type=products&lang=ru&action=delete&ID=24&return_url=%2Fproducts%2F";i:3;s:25:"Удалить товар";i:4;a:1:{s:7:"CONFIRM";s:76:"Вы уверены, что хотите удалить этот товар?";}}i:8;a:5:{i:0;s:13:"AddEditAction";i:1;i:23;i:2;s:163:"/bitrix/admin/cat_product_edit.php?IBLOCK_ID=2&type=products&ID=23&lang=ru&force_catalog=1&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Fproducts%2F";i:3;s:27:"Изменить товар";i:4;a:0:{}}i:9;a:5:{i:0;s:15:"AddDeleteAction";i:1;i:23;i:2;s:118:"/bitrix/admin/iblock_element_admin.php?IBLOCK_ID=2&type=products&lang=ru&action=delete&ID=23&return_url=%2Fproducts%2F";i:3;s:25:"Удалить товар";i:4;a:1:{s:7:"CONFIRM";s:76:"Вы уверены, что хотите удалить этот товар?";}}i:10;a:5:{i:0;s:13:"AddEditAction";i:1;i:22;i:2;s:163:"/bitrix/admin/cat_product_edit.php?IBLOCK_ID=2&type=products&ID=22&lang=ru&force_catalog=1&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Fproducts%2F";i:3;s:27:"Изменить товар";i:4;a:0:{}}i:11;a:5:{i:0;s:15:"AddDeleteAction";i:1;i:22;i:2;s:118:"/bitrix/admin/iblock_element_admin.php?IBLOCK_ID=2&type=products&lang=ru&action=delete&ID=22&return_url=%2Fproducts%2F";i:3;s:25:"Удалить товар";i:4;a:1:{s:7:"CONFIRM";s:76:"Вы уверены, что хотите удалить этот товар?";}}i:12;a:5:{i:0;s:13:"AddEditAction";i:1;i:21;i:2;s:163:"/bitrix/admin/cat_product_edit.php?IBLOCK_ID=2&type=products&ID=21&lang=ru&force_catalog=1&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Fproducts%2F";i:3;s:27:"Изменить товар";i:4;a:0:{}}i:13;a:5:{i:0;s:15:"AddDeleteAction";i:1;i:21;i:2;s:118:"/bitrix/admin/iblock_element_admin.php?IBLOCK_ID=2&type=products&lang=ru&action=delete&ID=21&return_url=%2Fproducts%2F";i:3;s:25:"Удалить товар";i:4;a:1:{s:7:"CONFIRM";s:76:"Вы уверены, что хотите удалить этот товар?";}}i:14;a:5:{i:0;s:13:"AddEditAction";i:1;i:20;i:2;s:163:"/bitrix/admin/cat_product_edit.php?IBLOCK_ID=2&type=products&ID=20&lang=ru&force_catalog=1&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Fproducts%2F";i:3;s:27:"Изменить товар";i:4;a:0:{}}i:15;a:5:{i:0;s:15:"AddDeleteAction";i:1;i:20;i:2;s:118:"/bitrix/admin/iblock_element_admin.php?IBLOCK_ID=2&type=products&lang=ru&action=delete&ID=20&return_url=%2Fproducts%2F";i:3;s:25:"Удалить товар";i:4;a:1:{s:7:"CONFIRM";s:76:"Вы уверены, что хотите удалить этот товар?";}}i:16;a:5:{i:0;s:13:"AddEditAction";i:1;i:19;i:2;s:163:"/bitrix/admin/cat_product_edit.php?IBLOCK_ID=2&type=products&ID=19&lang=ru&force_catalog=1&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Fproducts%2F";i:3;s:27:"Изменить товар";i:4;a:0:{}}i:17;a:5:{i:0;s:15:"AddDeleteAction";i:1;i:19;i:2;s:118:"/bitrix/admin/iblock_element_admin.php?IBLOCK_ID=2&type=products&lang=ru&action=delete&ID=19&return_url=%2Fproducts%2F";i:3;s:25:"Удалить товар";i:4;a:1:{s:7:"CONFIRM";s:76:"Вы уверены, что хотите удалить этот товар?";}}i:18;a:5:{i:0;s:13:"AddEditAction";i:1;i:18;i:2;s:163:"/bitrix/admin/cat_product_edit.php?IBLOCK_ID=2&type=products&ID=18&lang=ru&force_catalog=1&filter_section=0&bxpublic=Y&from_module=iblock&return_url=%2Fproducts%2F";i:3;s:27:"Изменить товар";i:4;a:0:{}}i:19;a:5:{i:0;s:15:"AddDeleteAction";i:1;i:18;i:2;s:118:"/bitrix/admin/iblock_element_admin.php?IBLOCK_ID=2&type=products&lang=ru&action=delete&ID=18&return_url=%2Fproducts%2F";i:3;s:25:"Удалить товар";i:4;a:1:{s:7:"CONFIRM";s:76:"Вы уверены, что хотите удалить этот товар?";}}}}}}';
return true;
?>