<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001449933941';
$dateexpire = '001485933941';
$ser_content = 'a:2:{s:7:"CONTENT";s:4447:"<div class="container">
<div class="box">
<div class="bx-newsdetail">
	<div class="bx-newsdetail-block" id="bx_1878455859_1">

				
			<h3 class="bx-newsdetail-title">Post Title</h3>
	
	<div class="bx-newsdetail-content">
			<p>С 20 по 23 апреля 2010 года состоится <strong>Мебельный Форум Беларуси</strong> &ndash; важнейшее мероприятии отрасли в текущем году. В экспозиции принимают участие свыше 160 компаний из <em>Беларуси, Австрии, Латвии, Литвы, России, Польши </em>и<em> Украины</em>. В составе форума состоится пять выставок:&quot;Минский мебельный салон&quot;, &quot;Мебельные технологии&quot;, &quot;ОфисКомфорт&quot;, &quot;Кухня&quot; и &quot;Домотех&quot;. Экспозиция будет строиться по принципу двух тематических секторов:<em> готовой мебели</em> и <em>материалов для ее производства</em>.
 
 
 
  <br />

 
 
 </p>

 
 
 
 
 
 
<p>В секторе готовой мебели будут представлены:</p>

 
 
 
 
 
 
<ul>
 
 
 
  <li>корпусная и мягкая мебель;</li>

 
 
 
 
 
 
  <li>мебель для детей и молодежи;</li>

 
 
 
 
 
 
  <li>предметы интерьера;</li>

 
 
 
 
 
 
  <li>кухонная мебель;</li>

 
 
 
 
 
 
  <li>мебель для офиса и административных зданий.</li>

 
 
 </ul>

 
 
 
 
 
 
<p>В секторе материалов для производства мебели будут демонстрироваться новинки рынка мебельной фурнитуры, материалов, обивочных тканей, элементов и аксессуаров для производства мебели.
 
 
  <br />

 
 
 </p>

 
 
 
 
<p>Помимо расширенной экспозиции пяти тематических выставок &quot;Экспофорум&quot; подготовил разнообразную деловую программу Мебельного форума. В рамках выставки состоятся семинары и мастер-классы. И поистине масштабным в этом году обещает стать республиканский конкурс форума &quot;<strong>Народное признание</strong>&quot;. В этом году он выходит за рамки выставки и становится республиканским смотром образцов мебели.
 
 
  <br />

 
 <strong>Мебельный Форум</strong> предоставляет специалистам возможность познакомиться с тенденциями мебельной моды, провести переговоры, получить практические предложения рынка, увидеть перспективы развития и новые конкурентные преимущества. Впервые для участников конкурса будет подготовлен маркетинговый отчет по результатам опроса посетителей выставок <strong>Мебельного Форума</strong>.
 
 
  <br />

 
 </p>

<p><em>Прием заявок на участие в конкурсе осуществляется до 12 апреля 2010 года.</em></p>

 
 
 
 
<ul>
 </ul>

 
 
 
 		</div>

		
			<div class="bx-newsdetail-date"><i class="fa fa-calendar-o"></i> 13.10.2013</div>
		
	<div class="row">
		<div class="col-xs-5">
		</div>
		</div>
	</div>
</div>
</div>
</div>
<script type="text/javascript">
	BX.ready(function() {
		var slider = new JCNewsSlider(\'bx_1878455859_1\', {
			imagesContainerClassName: \'bx-newsdetail-slider-container\',
			leftArrowClassName: \'bx-newsdetail-slider-arrow-container-left\',
			rightArrowClassName: \'bx-newsdetail-slider-arrow-container-right\',
			controlContainerClassName: \'bx-newsdetail-slider-control\'
		});
	});
</script>
";s:4:"VARS";a:2:{s:8:"arResult";a:11:{s:2:"ID";s:1:"1";s:9:"IBLOCK_ID";s:1:"1";s:4:"NAME";s:10:"Post Title";s:17:"IBLOCK_SECTION_ID";N;s:6:"IBLOCK";a:88:{s:2:"ID";s:1:"1";s:3:"~ID";s:1:"1";s:11:"TIMESTAMP_X";s:19:"12.12.2015 20:21:59";s:12:"~TIMESTAMP_X";s:19:"12.12.2015 20:21:59";s:14:"IBLOCK_TYPE_ID";s:4:"news";s:15:"~IBLOCK_TYPE_ID";s:4:"news";s:3:"LID";s:2:"s1";s:4:"~LID";s:2:"s1";s:4:"CODE";s:17:"furniture_news_s1";s:5:"~CODE";s:17:"furniture_news_s1";s:4:"NAME";s:14:"Новости";s:5:"~NAME";s:14:"Новости";s:6:"ACTIVE";s:1:"Y";s:7:"~ACTIVE";s:1:"Y";s:4:"SORT";s:3:"500";s:5:"~SORT";s:3:"500";s:13:"LIST_PAGE_URL";s:6:"/news/";s:14:"~LIST_PAGE_URL";s:6:"/news/";s:15:"DETAIL_PAGE_URL";s:21:"#SITE_DIR#/news/#ID#/";s:16:"~DETAIL_PAGE_URL";s:21:"#SITE_DIR#/news/#ID#/";s:16:"SECTION_PAGE_URL";N;s:17:"~SECTION_PAGE_URL";N;s:18:"CANONICAL_PAGE_URL";s:0:"";s:19:"~CANONICAL_PAGE_URL";s:0:"";s:7:"PICTURE";N;s:8:"~PICTURE";N;s:11:"DESCRIPTION";s:0:"";s:12:"~DESCRIPTION";s:0:"";s:16:"DESCRIPTION_TYPE";s:4:"text";s:17:"~DESCRIPTION_TYPE";s:4:"text";s:7:"RSS_TTL";s:2:"24";s:8:"~RSS_TTL";s:2:"24";s:10:"RSS_ACTIVE";s:1:"Y";s:11:"~RSS_ACTIVE";s:1:"Y";s:15:"RSS_FILE_ACTIVE";s:1:"N";s:16:"~RSS_FILE_ACTIVE";s:1:"N";s:14:"RSS_FILE_LIMIT";s:1:"0";s:15:"~RSS_FILE_LIMIT";s:1:"0";s:13:"RSS_FILE_DAYS";s:1:"0";s:14:"~RSS_FILE_DAYS";s:1:"0";s:17:"RSS_YANDEX_ACTIVE";s:1:"N";s:18:"~RSS_YANDEX_ACTIVE";s:1:"N";s:6:"XML_ID";s:17:"furniture_news_s1";s:7:"~XML_ID";s:17:"furniture_news_s1";s:6:"TMP_ID";s:32:"bc3c156c69d32effd3cc2f1cfcc0a8b7";s:7:"~TMP_ID";s:32:"bc3c156c69d32effd3cc2f1cfcc0a8b7";s:13:"INDEX_ELEMENT";s:1:"Y";s:14:"~INDEX_ELEMENT";s:1:"Y";s:13:"INDEX_SECTION";s:1:"N";s:14:"~INDEX_SECTION";s:1:"N";s:8:"WORKFLOW";s:1:"N";s:9:"~WORKFLOW";s:1:"N";s:7:"BIZPROC";s:1:"N";s:8:"~BIZPROC";s:1:"N";s:15:"SECTION_CHOOSER";s:1:"L";s:16:"~SECTION_CHOOSER";s:1:"L";s:9:"LIST_MODE";s:0:"";s:10:"~LIST_MODE";s:0:"";s:11:"RIGHTS_MODE";s:1:"S";s:12:"~RIGHTS_MODE";s:1:"S";s:16:"SECTION_PROPERTY";s:1:"N";s:17:"~SECTION_PROPERTY";s:1:"N";s:14:"PROPERTY_INDEX";s:1:"N";s:15:"~PROPERTY_INDEX";s:1:"N";s:7:"VERSION";s:1:"1";s:8:"~VERSION";s:1:"1";s:17:"LAST_CONV_ELEMENT";s:1:"0";s:18:"~LAST_CONV_ELEMENT";s:1:"0";s:15:"SOCNET_GROUP_ID";N;s:16:"~SOCNET_GROUP_ID";N;s:16:"EDIT_FILE_BEFORE";s:0:"";s:17:"~EDIT_FILE_BEFORE";s:0:"";s:15:"EDIT_FILE_AFTER";s:0:"";s:16:"~EDIT_FILE_AFTER";s:0:"";s:13:"SECTIONS_NAME";s:14:"Разделы";s:14:"~SECTIONS_NAME";s:14:"Разделы";s:12:"SECTION_NAME";s:12:"Раздел";s:13:"~SECTION_NAME";s:12:"Раздел";s:13:"ELEMENTS_NAME";s:14:"Новости";s:14:"~ELEMENTS_NAME";s:14:"Новости";s:12:"ELEMENT_NAME";s:14:"Новость";s:13:"~ELEMENT_NAME";s:14:"Новость";s:11:"EXTERNAL_ID";s:17:"furniture_news_s1";s:12:"~EXTERNAL_ID";s:17:"furniture_news_s1";s:8:"LANG_DIR";s:1:"/";s:9:"~LANG_DIR";s:1:"/";s:11:"SERVER_NAME";s:0:"";s:12:"~SERVER_NAME";s:0:"";}s:13:"LIST_PAGE_URL";s:6:"/news/";s:14:"~LIST_PAGE_URL";s:6:"/news/";s:11:"SECTION_URL";s:0:"";s:7:"SECTION";a:1:{s:4:"PATH";a:0:{}}s:16:"IPROPERTY_VALUES";a:0:{}s:11:"TIMESTAMP_X";s:19:"12.12.2015 20:32:05";}s:18:"templateCachedData";a:5:{s:13:"additionalCSS";s:71:"/bitrix/templates/.default/components/bitrix/news.detail/flat/style.css";s:12:"additionalJS";s:71:"/bitrix/templates/.default/components/bitrix/news.detail/flat/script.js";s:9:"frameMode";b:1;s:11:"externalCss";a:3:{i:0;s:30:"/bitrix/css/main/bootstrap.css";i:1;s:33:"/bitrix/css/main/font-awesome.css";i:2;s:83:"/bitrix/templates/.default/components/bitrix/news.detail/flat/themes/blue/style.css";}s:8:"__NavNum";i:1;}}}';
return true;
?>