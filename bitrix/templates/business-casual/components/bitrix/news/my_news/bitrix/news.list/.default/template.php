<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container">

  <div class="row">

    <div class="box">
      <div class="col-lg-12">
        <hr>
        <h2 class="intro-text text-center">Company
          <strong>blog</strong>
        </h2>
        <hr>
      </div>
      <?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
      <?endif;?>
      <?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	  $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="col-lg-12 text-center" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
	  <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
	    <?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>

		<img
			class="img-responsive img-border img-full"
			border="0"
			src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
			width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
			height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
			alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
			title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
		/>
	    <?else:?>
		<img
			class="img-responsive img-border img-full"
			border="0"
			src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
			width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
			height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
			alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
			title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
		/>
	    <?endif;?>
	    <?endif?>
	    <h2>
	      <?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
		<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
		  <?echo $arItem["NAME"]?>
		<?else:?>
		  <?echo $arItem["NAME"]?>
		<?endif;?>
	      <?endif;?>
	      <br />
	      <small>
		<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
		  <?echo $arItem["DISPLAY_ACTIVE_FROM"]?>
		  <?endif?>
	      </small>
	    </h2>
	    <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
	      <p><?echo $arItem["PREVIEW_TEXT"];?></p>
	    <?endif;?>
	    <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
	      <div style="clear:both"></div>
	      <?endif?>
	      <?foreach($arItem["FIELDS"] as $code=>$value):?>
		<small>
		  <?=GetMessage("IBLOCK_FIELD_".$code)?>: <?=$value;?>
		</small><br />
	      <?endforeach;?>
	      <?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
		<small>
		  <?=$arProperty["NAME"]?>: 
		  <?if(is_array($arProperty["DISPLAY_VALUE"])):?>
		    <?=implode(" / ", $arProperty["DISPLAY_VALUE"]);?>
		  <?else:?>
		    <?=$arProperty["DISPLAY_VALUE"];?>
		    <?endif?>
		</small><br />
	      <?endforeach;?>
	      <?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
		<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
		  <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>" class="btn btn-default btn-lg">Read More</a><br />
		<?else:?>
		  Read More<br />
		<?endif;?>
	      <?endif;?>
	      <hr>
	</div>

      <?endforeach;?>
      <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
      <?endif;?>
    </div>
  </div>
</div>
